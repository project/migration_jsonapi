<?php

declare(strict_types = 1);

namespace Drupal\migration_jsonapi\Plugin\migrate_plus\data_parser;

use Drupal\migrate\MigrateException;
use Drupal\migrate_plus\Plugin\migrate_plus\data_parser\Json;

/**
 * This plugin parses JsonAPI data by adding new parameters to the JSON parser.
 *
 * The parser requires an empty url parameter.  The urls will be automatically
 * generated from the following parameters:
 *   jsonapi_host
 *   jsonapi_prefix
 *   jsonapi_endpoint
 *   jsonapi_query_params.
 *
 * The urls generated will be batched based on the jsonapi_query_param
 * page[limit] and defaults to a limit of 50.
 *
 * Available configuration keys:
 * - jsonapi_host: The Drupal host that we will be querying jsonapi data for.
 * - jsonapi_prefix: The path to Jsonapi
 * - jsonapi_endpoint: The Drupal Jsonapi endpoint that will be queried.
 *   The endpoint must start with a '/'.
 * - jsonapi_query_params: (optional) An array which provides
 *   the query parameters to be used.  For example,
 *   The following will create the query parameter "page[limit]=10":
 * @code
 *   jsonapi_query_params:
 *     page
 *       limit: 10
 * @endcode
 *
 * - jsonapi_langcodes: (optional) An array of langcodes
 *   If the jsonapi_langcodes are not present then the default language will be
 *   used in the query.
 *
 *   To include the default language along with other langcodes, use "und"
 *   in your jsonapi_langcodes array.
 *
 *   If the jsonapi_langcodes are available, then the url endpoint will be
 *   prepended with the langcode and added to the query parameters as
 *   "filter[langcode]={langcode}"
 *
 * @codingStandardsIgnoreStart
 *
 * Example usage with base language:
 * @code
 * source:
 *   plugin: url
 *   data_fetcher_plugin: http
 *   data_parser_plugin: jsonapi
 *   item_selector: data
 *   jsonapi_host: {source host}
 *   jsonapi_prefix: '/jsonapi'
 *   jsonapi_endpoint: '/node/article'
 *   jsonapi_query_params:
 *     filter:
 *       default_langcode: 1
 *   urls: []
 * @endcode
 *
 * Example usage with language support:
 * @code
 * source:
 *   plugin: url
 *   data_fetcher_plugin: http
 *   data_parser_plugin: jsonapi
 *   item_selector: data
 *   jsonapi_host: {source host}
 *   jsonapi_prefix: '/jsonapi'
 *   jsonapi_endpoint: '/node/article'
 *   jsonapi_query_params:
 *     filter:
 *       default_langcode: 0
 *   jsonapi_langcodes:
 *     - ja
 *     - de
 *   urls: []
 * @endcode
 *
 * @codingStandardsIgnoreEnd
 *
 * @DataParser(
 *   id = "jsonapi",
 *   title = @Translation("JSONAPI")
 * )
 */
class Jsonapi extends Json {

  /**
   * Stores the JSONAPI Url without the page[offset] query parameter.
   *
   * @var string
   */
  protected $jsonapiHost;

  /**
   * Stores the JSONAPI prefix..
   *
   * @var string
   */
  protected $jsonapiPrefix;

  /**
   * Stores the JSONAPI resource path.
   *
   * @var string
   */
  protected $jsonapiEndpoint;

  /**
   * Stores the JSONAPI filters, pagers, offsets, etc (query data).
   *
   * @var string
   */
  protected $jsonapiQueryString;

  /**
   * Keeps track of the current page[offset] query parameter.
   *
   * @var int
   */
  protected $pageOffset = 0;

  /**
   * Allows for the migration to override the pageOffset parameter name.
   *
   * @var mixed|string
   */
  protected $pageOffsetParameter = 'page[offset]';

  /**
   * Stores the page[limit] value to be used as batch size of the import.
   *
   * @var int
   */
  protected $pageLimit = 50;

  /**
   * Allows for the migration to override the pageLimit parameter name.
   *
   * @var mixed|string
   */
  protected $pageLimitParameter = 'page[limit]';

  /**
   * Stores languages.
   *
   * @var int
   */
  protected $jsonapiLangcodes = [];

  /**
   * Allows for the migration to override the language langcode parameter name.
   *
   * @var mixed|string
   */
  protected $languageParameter = 'filter[langcode]';

  /**
   * Allows for the migration to override the empty langcode value.
   *
   * @var mixed|string
   */
  protected $emptyLangcode = 'und';

  /**
   * Allows for the migration to override whether the langcode should be
   *   prepended to the url.
   *
   * @var bool|mixed
   */
  protected $prependLangcode = TRUE;

  /**
   * Stores the current language index.
   *
   * @var int
   */
  protected $jsonapiLangcodeIndex = NULL;

  /**
   * Stores the decoded full url response.
   *
   * @var array
   */
  protected $sourceGlobal = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {

  // Set jsonapi properties from migration source configuration

    if (empty($configuration['jsonapi_host'])) {
      throw new MigrateException('Migration parameter "jsonapi_host" is required.');
    }
    else {
      $this->jsonapiHost = $configuration['jsonapi_host'];
    }

    if (!isset($configuration['jsonapi_prefix'])) {
      throw new MigrateException('JsonAPI prefix not set.');
    }

    $this->jsonapiPrefix = $configuration['jsonapi_prefix'];

    if (!isset($configuration['jsonapi_endpoint'])) {
      throw new MigrateException('JsonAPI endpoint not set.');
    }

    $this->jsonapiEndpoint = $configuration['jsonapi_endpoint'];

    if (isset($configuration['jsonapi_langcodes']) && !empty($configuration['jsonapi_langcodes'])) {
      $this->jsonapiLangcodes = $configuration['jsonapi_langcodes'];
      $this->jsonapiLangcodeIndex = 0;
    }

    $this->emptyLangcode = $configuration['empty_langcode'] ?? $this->emptyLangcode;
    $this->prependLangcode = $configuration['prepend_langcode'] ?? $this->prependLangcode;

    $query_params = [];
    if (isset($configuration['jsonapi_query_params'])) {
      foreach ($configuration['jsonapi_query_params'] as $param_name => $param_data) {
        $query_params += $this->getSubQueryParams($param_name, $param_data);
      }
    }

    if (isset($configuration['jsonapi_query_param_keys'])) {
      $this->pageOffsetParameter = $configuration['jsonapi_query_param_keys']['page_offset'] ?? $this->pageOffsetParameter;
      $this->pageLimitParameter = $configuration['jsonapi_query_param_keys']['page_limit'] ?? $this->pageLimitParameter;
      $this->languageParameter = $configuration['jsonapi_query_param_keys']['language'] ?? $this->languageParameter;

    }

    if (isset($query_params[$this->pageOffsetParameter])) {
      $this->pageOffset = $query_params[$this->pageOffsetParameter];

      // Page offset will be set later when the url is utilized.
      unset($query_params[$this->pageOffsetParameter]);
    }

    if (isset($query_params[$this->pageLimitParameter])) {
      $this->pageLimit = $query_params[$this->pageLimitParameter];
    }
    else {
      $query_params[$this->pageLimitParameter] = $this->pageLimit;
    }

    // Generate query string for url.
    $query_param_vals = [];
    foreach ($query_params as $param => $param_value) {
      $query_param_vals[] = $param . '=' . $param_value;
    }
    $query_string = implode('&', $query_param_vals);

    $this->jsonapiQueryString = '?' . $query_string;

    parent::__construct($configuration, $plugin_id, $plugin_definition);

  }

  /**
   * Creates JSONAPI Url from current class properties.
   *
   * @return string|null
   *   The generated URL.
   */
  private function getJsonapiUrl() {
    // Initialize url with host value.
    $url = $this->jsonapiHost;

    // Determine langcode value.
    if ($this->jsonapiLangcodeIndex === NULL) {
      $langcode = $this->emptyLangcode;
    }
    elseif (isset($this->jsonapiLangcodes[$this->jsonapiLangcodeIndex])) {
      $langcode = $this->jsonapiLangcodes[$this->jsonapiLangcodeIndex];

      // Prepend langcode to endpoint.
      if ($this->prependLangcode && $langcode != $this->emptyLangcode) {
        $url .= '/' . $langcode;
      }

    }
    else {
      return NULL;
    }

    // Add endpoint and static query parameters.
    $url .= $this->jsonapiPrefix . $this->jsonapiEndpoint . $this->jsonapiQueryString;

    // Add langcode query parameter.
    if ($langcode != $this->emptyLangcode) {
      $url .= '&' . $this->languageParameter . '=' . $langcode;
    }

    // Add page offset query parameter.
    $url .= '&' . $this->pageOffsetParameter . '=' . $this->pageOffset;

    return $url;
  }

  /**
   * {@inheritdoc}
   */
  protected function nextSource(): bool {

    // Define langcode.
    $url = $this->getJsonapiUrl();
    if ($url === NULL) {
      return FALSE;
    }

    // Add url to urls array.
    $this->urls[] = $url;

    // Increment page offset.
    $this->pageOffset += $this->pageLimit;

    // @see \Drupal\migrate_plus\DataParserPluginBase::nextSource
    while (
      is_null($this->activeUrl) ||
      (count($this->urls) - 1) > $this->activeUrl
    ) {

      if (is_null($this->activeUrl)) {
        $this->activeUrl = 0;
      }
      else {
        // Increment the activeUrl so we try to load the next source.
        ++$this->activeUrl;
        if ($this->activeUrl >= count($this->urls)) {
          return FALSE;
        }
      }

      // Log the urls being queried.
      \Drupal::logger('jsonapi')->notice($this->urls[$this->activeUrl]);

      if ($this->openSourceUrl($this->urls[$this->activeUrl])) {
        // We have a valid source.
        // Make sure source is not empty.
        if ($this->iterator->count()) {
          return TRUE;
        }

        // If we are here that means we found no results from the Url.
        // We need to reset our pageOffset and iterate to the next item
        // in the jsonapiLangcodes array.
        $this->pageOffset = 0;

        // Increment jsonapiLangcodeIndex.
        if ($this->jsonapiLangcodeIndex !== NULL) {
          $this->jsonapiLangcodeIndex++;
        }
        else {
          // If LangcodeIndex == NULL we are importing the original translation
          // and should stop execution.
          return FALSE;
        }

        // Here we are removing the url which has no content.
        // May not be needed.
        array_pop($this->urls);
        --$this->activeUrl;

        // Add new url.
        $url = $this->getJsonapiUrl();
        if ($url === NULL) {
          return FALSE;
        }

        // Add url to urls array.
        $this->urls[] = $url;

        // Increment page offset.
        $this->pageOffset += $this->pageLimit;

      }
    }
    return FALSE;
  }

  /**
   * Recursive function which returns the query param name based on the values
   * passed.
   *
   * @param string $param_name
   * @param string|array $param_values
   *
   * @return array
   */
  private function getSubQueryParams($param_name, $param_values) {
    if (!is_array($param_values)) {
      return [$param_name => $param_values];
    }

    $query_param_names = [];
    foreach ($param_values as $name => $value) {
      $query_param_names += $this->getSubQueryParams($param_name . '[' . $name . ']', $value);
    }
    return $query_param_names;
  }
}
